<?php

namespace Rams\Orderdata\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;


class GetOrderData implements ObserverInterface
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            file_put_contents('/var/www/html/event.log', 'getevent', FILE_APPEND);
            $orderMagento = $observer->getEvent()->getOrder();
            file_put_contents('/var/www/html/event.log', $orderMagento->getCustomerEmail() . "\r\n", FILE_APPEND);
            $total = $orderMagento->getGrandTotal();//this will log order total
            $this->logger->info($total);
            $order = new \stdClass();

            $order->billing = ["firstname" => '', "lastname" => '', "company" => '', "street_address" => '', "suburb" => '', "city" => '', "postcode" => '', "state" => '', "country" => '', "title" => '', "format_id" => ''];
            $order->customer = ["firstname" => $orderMagento->getCustomerFirstname(), "lastname" => $orderMagento->getCustomerLastname(), "company" => '', "street_address" => '', "suburb" => '', "city" => '', "postcode" => '', "state" => '', "country" => '', "title" => '', "telephone" => '', "fax" => '',
                "email_address" => $orderMagento->getCustomerEmail(), "format_id" => '', "id" => $orderMagento->getCustomerId()];
            $order->delivery = ["firstname" => '', "lastname" => '', "company" => '', "street_address" => '', "suburb" => '', "city" => '', "postcode" => '', "state" => '', "country" => '', "telephone" => '', "email_address" => '', "format_id" => ''];
            $order->info = ["payment_method" => '', "cc_type" => '', "cc_owner" => '', "cc_number" => '', "cc_expires" => '', "order_status" => '', "currency" => '', "currency_value" => '', "vipcode" => '', "comments" => ''];
            $order->shipping = ["id" => ''];

            $Products = array();
            foreach ($orderMagento->getAllVisibleItems() as $item) {
                $sku = $item->getSku();
                $idNew = -1;
                $mysqli = mysqli_connect("pegasus-website-rds-mysql-instance.ckjgo9xarrqv.us-east-1.rds.amazonaws.com", "pegasusdbaccess", "stoy1rE2og9iT2", "plshop");
                if (mysqli_connect_errno()) {
                    file_put_contents('/var/www/html/event.log', "Не удалось подключиться к MySQL: " . mysqli_connect_error(), FILE_APPEND);
                }
                $res = $mysqli->query("select products_id  from products where products_model ='" . $sku . "' limit 1 ");
                file_put_contents('/var/www/html/event.log', "select products_id from products where products_model ='" . $sku . "' limit 1 ", FILE_APPEND);
                if ($res) {
                    $row = $res->fetch_assoc();
                    if (isset($row['products_id'])) {
                        $idNew = $row['products_id'];
                    } else {
                        $idNew = 0;
                    }
                }
                if ($idNew > 0) {
                    $id = $idNew;
                } else {
                    $id = 0;
                }
                $Products[] = ['id' => $id, 'idMagento' => $item->getProductId(), 'qty' => $item->getQtyToShip(), 'qty2' => $item->getQtyOrdered(), 'sku' => $item->getSku(), 'idNew' => $idNew];
            }
            $order->products = $Products;
            file_put_contents('/var/www/html/event.log', print_r($order, true), FILE_APPEND);
            include('/var/www/html/ixms/includes/shopHookMagento.php');
        } catch (\Exception $e) {
            file_put_contents('/var/www/html/event.log', 'error ' . $e->getMessage(), FILE_APPEND);
            $this->logger->info($e->getMessage());
            exit();
        }
    }
}
